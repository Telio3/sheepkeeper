from tkinter import *

from entities.door import Door
from entities.drone import Drone
from entities.sheep import Sheep

from threading import Timer


def init():
    # init window
    window = Tk()
    window.title('SheepKeeper')
    window.resizable(False, False)
    icon = PhotoImage(file="./assets/drone.png")
    window.iconphoto(False, icon)

    # init Canvas
    canvasHeight = 500
    canvasWidth = 500
    canvas = Canvas(window, height=canvasHeight, width=canvasWidth, bg="grey", highlightthickness=0)
    canvas.pack()

    # init Fence
    marginFence = 70
    fenceHeight = canvasHeight - marginFence * 2
    fenceWidth = canvasWidth - marginFence * 2
    canvas.create_rectangle(marginFence, marginFence, fenceWidth + marginFence, fenceHeight + marginFence, fill="green",
                            outline="black")

    # init Door
    door = Door(300, 70, 350, 90)
    canvas.create_rectangle(door.x, door.y, door.length, door.width, fill="white", outline="white")

    # init Sheep
    sheep = Sheep(canvas, 200, 200)
    sheep.element = canvas.create_image(sheep.x, sheep.y, anchor=NW, image=sheep.image)

    # init Drones
    drones = [
        Drone(canvas, 120, canvasHeight - 60),
        Drone(canvas, 220, canvasHeight - 60),
        Drone(canvas, 320, canvasHeight - 60)
    ]

    for drone in drones:
        drone.element = canvas.create_image(drone.x, drone.y, anchor=NW, image=drone.image)

    setTimeout(drones[0].moveTo, 1000, 100, 270)
    setTimeout(drones[1].moveTo, 1000, 200, 270)
    setTimeout(drones[2].moveTo, 1000, 300, 270)

    setTimeout(drones[0].moveTo, 2000, 100, 200)

    setTimeout(sheep.moveTo, 3000, 300, 200)
    setTimeout(drones[0].moveTo, 3000, 200, 200)
    setTimeout(drones[1].moveTo, 3000, 280, 270)
    setTimeout(drones[2].moveTo, 3000, 380, 230)

    setTimeout(sheep.moveTo, 4000, 300, 100)
    setTimeout(drones[0].moveTo, 4000, 200, 100)
    setTimeout(drones[1].moveTo, 4000, 280, 170)
    setTimeout(drones[2].moveTo, 4000, 380, 130)

    setTimeout(sheep.moveTo, 4500, 300, 10)

    setTimeout(drones[0].moveTo, 5500, drones[0].originalX, drones[0].originalY)
    setTimeout(drones[1].moveTo, 5500, drones[1].originalX, drones[0].originalY)
    setTimeout(drones[2].moveTo, 5500, drones[2].originalX, drones[0].originalY)

    window.mainloop()


def setTimeout(fn, ms, *args, **kwargs):
    t = Timer(ms / 1000., fn, args=args, kwargs=kwargs)
    t.start()
    return t
