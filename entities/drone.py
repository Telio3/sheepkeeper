from PIL import Image, ImageTk


class Drone:
    def __init__(self, canvas, x, y):
        self.element = None
        self.canvas = canvas
        self.x = x
        self.originalX = x
        self.y = y
        self.originalY = y
        self.image = ImageTk.PhotoImage(Image.open("./assets/drone.png").resize((50, 50)))

    def up(self):
        self.y -= 10
        self.canvas.move(self.element, 0, -10)

    def down(self):
        self.y += 10
        self.canvas.move(self.element, 0, 10)

    def left(self):
        self.x -= 10
        self.canvas.move(self.element, -10, 0)

    def right(self):
        self.x += 10
        self.canvas.move(self.element, 10, 0)

    def moveTo(self, x, y):
        if self.x != x:
            if x - self.x > 0:
                self.right()
            else:
                self.left()

            self.canvas.after(40, self.moveTo, x, y)
            return

        if self.y != y:
            if y - self.y > 0:
                self.down()
            else:
                self.up()

            self.canvas.after(40, self.moveTo, x, y)
            return
